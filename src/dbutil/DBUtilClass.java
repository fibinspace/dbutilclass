package dbutil;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DBUtilClass {
    private static String jdbcUrl;
    private static boolean isInitialized;

    private static void initialize(){
        if(!isInitialized)
        {
             try {
                 Properties properties = new Properties();
                 InputStream source = DBUtilClass.class.getResourceAsStream("jdbc.properties");
                 properties.load(source);
                 source.close();
                 Class.forName(properties.getProperty("driverClass"));
                 jdbcUrl = properties.getProperty("jdbcUrl");
                 isInitialized = true;
             }
            catch (Exception e)
            {
                System.err.println("Error while initializing JDBC");
            }
        }

    }

    public static Connection getConnection() throws Exception
    {

        initialize(); //wywolujemy initialajz metod, to nas upewnia, ze metoda jest wywolana tylko raz

        return DriverManager.getConnection(jdbcUrl);
    }

    public static void Close(Connection conn) throws Exception
    {
        if(conn != null)
            conn.close();
    }
}
